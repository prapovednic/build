package com.fa13.build.api;

import java.util.Properties;

public interface IStorable {

    public void store(Properties props);
    
}