package com.fa13.build.controller.io;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;

import com.fa13.build.model.game.PlayerPosition;

public class SchemeWriter {
    public static final Charset UTF8_CHARSET = Charset.forName("UTF-8");
    
    public static final void write(String fileName, Map<String, PlayerPosition[]> schemes) throws IOException {
/*
        HashingOutputStream stream = new HashingOutputStream(new FileOutputStream(fileName));
        String s = createContents(schemes);
        stream.write(s.getBytes(HashingOutputStream.WIN_CHARSET));
        stream.close();
*/
        String s = createContents(schemes);
        FileUtils.writeStringToFile( new File(fileName), s, UTF8_CHARSET );
    }

    private static String createContents(Map<String, PlayerPosition[]> schemes) {
        if (schemes == null || schemes.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();

        for (Entry<String, PlayerPosition[]> entry : schemes.entrySet()) {
            if (entry.getValue() != null && entry.getKey() != null) {
                sb.append(entry.getKey()).append(AbstractFormWriter.LINE_BREAK);
                for (PlayerPosition pp : entry.getValue()) {
                    if (!pp.isGoalkeeper()) {

                        sb.append(pp.getAmplua().toString()).append(" ");

                        sb.append(pp.getDefenceX()).append(" ").append(pp.getDefenceY()).append(" ");
                        sb.append(pp.getAttackX()).append(" ").append(pp.getAttackY()).append(" ");
                        sb.append(pp.getFreekickX()).append(" ").append(pp.getFreekickY()).append(AbstractFormWriter.LINE_BREAK);
                    }
                }
            }

        }

        return sb.toString();
    }

}