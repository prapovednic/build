package com.fa13.build.controller.io;

import com.fa13.build.model.PlayerAmplua;
import com.fa13.build.model.game.GameForm;
import com.fa13.build.model.game.PlayerPosition;
import com.fa13.build.model.game.PlayerSubstitution;
import com.fa13.build.model.game.TeamTactics;
import com.fa13.build.view.MainWindow;

import java.util.Locale;

/**
 * Stateless writer for a {@link GameForm}.
 */
public class GameWriter extends AbstractFormWriter<GameForm> {

    public static final String UNDEFINED_SUBSTITUTION = "0 0 -20 20 0 0 -1 НН";
    public static final String UNDEFINED_HALFTIME_CHANGE = "1 0 тактика агрессивность стиль";
    public static final String UNDEFINED_AMPLUA = "НН";

    @Override
    protected void appendFormData(StringBuilder contentBuilder, GameForm form) {
        appendLine(contentBuilder, form.getTournamentID());
        appendLine(contentBuilder, form.getGameType());
        appendLine(contentBuilder, MainWindow.getAllInstance().getTeamById(form.getTeamID()).getName());
        appendLine(contentBuilder, form.getPassword() == null ? "" : form.getPassword());

        PlayerPosition[] firstTeam = form.getFirstTeam();
        appendLine(contentBuilder, createGameSchema(firstTeam));
        appendLine(contentBuilder, form.getStartTactics().getTeamTactic().toString());
        appendLine(contentBuilder, form.getStartTactics().getTeamHardness().toString());
        appendLine(contentBuilder, form.getStartTactics().getTeamStyle().toString());
        appendLine(contentBuilder, form.getTeamCoachSettings().toString());

        for (PlayerPosition player : firstTeam) {
            player.validatePersonalSettings();
            appendLine(contentBuilder, formatPlayerPosition(player));
        }
        appendLine(contentBuilder, "запасные:");
        for (int bench : form.getBench()) {
            appendLine(contentBuilder, bench);
        }

        appendLine(contentBuilder, "стандарты:");
        appendLine(contentBuilder, standardLineCaptain(firstTeam));
        appendLine(contentBuilder, standardLinePenalty(firstTeam));
        appendLine(contentBuilder, standardLineFreeKick(firstTeam));
        appendLine(contentBuilder, standardLineIndirectFreeKick(firstTeam));
        appendLine(contentBuilder, standardLineLeftCorner(firstTeam));
        appendLine(contentBuilder, standardLineRightCorner(firstTeam));

        appendLine(contentBuilder, "пенальти:");
        appendLine(contentBuilder, formatPenalties(firstTeam));

        appendLine(contentBuilder, "цена билета:");
        appendLine(contentBuilder, form.getPrice());

        appendLine(contentBuilder, "замены:");
        PlayerSubstitution[] substitutions = form.getSubstitutions();
        for (PlayerSubstitution substitution : substitutions) {
            if (substitution.getNumber() == 0) {
                appendLine(contentBuilder, UNDEFINED_SUBSTITUTION);
            } else {
                substitution.validatePersonalSettings();
                appendLine(contentBuilder, formatPlayerSubstitution(substitution));
            }
        }
        appendLine(contentBuilder, form.getSubstitutionPreferences().ordinal());

        appendLine(contentBuilder, "глобал:");
        appendLine(contentBuilder, String.format("%d %d %d", form.getDefenceTime(), form.getDefenceMin(), form.getDefenceMax()));
        appendLine(contentBuilder, String.format("%d %d %d", form.getAttackTime(), form.getAttackMin(), form.getAttackMax()));

        for (TeamTactics halftimeTeamTacticChanges : form.getHalftimeChanges()) {
            if (halftimeTeamTacticChanges == null) {
                appendLine(contentBuilder, UNDEFINED_HALFTIME_CHANGE);
            } else {
                appendLine(contentBuilder, formatHalftimeTeamTackticChanges(halftimeTeamTacticChanges));
            }
        }
    }

    private StringBuilder formatPenalties(PlayerPosition[] firstTeam) {
        StringBuilder sb = new StringBuilder();
        for (int penalty : createPenalties(firstTeam)) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(penalty);
        }
        return sb;
    }

    @Override
    protected String[] getAdditionalDefenceValues(GameForm form) {
        return new String[] {
            form.getTeamID()
        };
    }

    private String createGameSchema(PlayerPosition[] players) {
        int defNum = 0;
        int midNum = 0;
        int forNum = 0;
        for (PlayerPosition player : players) {
            PlayerAmplua amp = player.getAmplua();
            switch (amp) {
                case LD:
                case CD:
                case RD:
                    defNum++;
                    break;
                case LM:
                case CM:
                case RM:
                    midNum++;
                    break;
                case LF:
                case CF:
                case RF:
                    forNum++;
                    break;
                default:
                    break;
            }
        }
        return String.format("M-%d-%d-%d", defNum, midNum, forNum);
    }

    private int[] createPenalties(PlayerPosition[] players) {
        int penalties[] = new int[11];
        for (PlayerPosition player : players) {
            int curr = player.getPenaltyOrder();
            if (curr != 0) {
                penalties[curr - 1] = player.getNumber();
            }
        }
        return penalties;
    }

    private StringBuilder formatPlayerPosition(PlayerPosition player) {
        StringBuilder sb = new StringBuilder();
        sb.append(player.getAmplua().toString()).append(" ");
        sb.append(player.getNumber());

        sb.append(formatMainPlayerCoordinates(player));
        sb.append(" -60.0 0.0");
        sb.append(formatFreeKickPlayerCoordinates(player));

        sb.append(formatPersonalSettings(player));
        sb.append(" 3 3 3 3");
        return sb;
    }

    private StringBuilder formatPlayerSubstitution(PlayerSubstitution substitution) {
        StringBuilder sb = new StringBuilder();
        sb.append(formatSubstitutionSettings(substitution));
        sb.append(' ').append(substitution.getSpecialRole() == 0 ? -1 : substitution.getSpecialRole());

        if (substitution.isUseCoordinates()) {
            sb.append(formatMainPlayerCoordinates(substitution));
            sb.append(formatFreeKickPlayerCoordinates(substitution));
            sb.append(formatPersonalSettings(substitution));
        } else {
            PlayerAmplua amplua = substitution.getAmplua();
            sb.append(' ').append(amplua != null ? amplua.getFormValue() : UNDEFINED_AMPLUA);
        }

        return sb;
    }

    private String formatSubstitutionSettings(PlayerSubstitution substitution) {
        return String.format("0 %d %d %d %d %d",
            substitution.getTime(),
            substitution.getMinDifference(),
            substitution.getMaxDifference(),
            substitution.getSubstitutedPlayer(),
            substitution.getNumber()
        );
    }

    private String formatMainPlayerCoordinates(PlayerPosition playerPosition) {
        return String.format(Locale.US, " %.1f %.1f %.1f %.1f",
            playerPosition.getDefenceX() * 0.2 - 60,  playerPosition.getDefenceY() * 0.2 - 45,
            playerPosition.getAttackX() * 0.2 - 60,   playerPosition.getAttackY() * 0.2 - 45
        );
    }

    private String formatFreeKickPlayerCoordinates(PlayerPosition playerPosition) {
        return String.format(Locale.US, " %.1f %.1f",
            playerPosition.getFreekickX() * 0.2 - 60, playerPosition.getFreekickY() * 0.2 - 45
        );
    }

    private StringBuilder formatPersonalSettings(PlayerPosition player) {
        StringBuilder sb = new StringBuilder();
        sb.append(player.isLongShot() ? " 1" : " 0");
        sb.append(' ').append(player.getActOnFreekick().getFormValue());
        sb.append(player.isFantasista() ? " 1" : " 0");
        sb.append(player.isDispatcher() ? " 1" : " 0");
        sb.append(' ').append(player.getPersonalDefence());
        sb.append(player.isPressing() ? " 1" : " 0");
        sb.append(player.isKeepBall() ? " 1" : " 0");
        sb.append(player.isDefenderAttack() ? " 1" : " 0");
        sb.append(' ').append(player.getPassingStyle().getFormValue());
        sb.append(' ').append(player.getHardness().getPlayerFormValue());
        return sb;
    }

    private String standardLineCaptain(PlayerPosition[] firstTeam) {
        int main = 0;
        int assist = 0;
        for (PlayerPosition player : firstTeam) {
            if (player.isCaptain()) {
                main = player.getNumber();
            }
            if (player.isCaptainAssistant()) {
                assist = player.getNumber();
            }
        }
        return String.format("%d %d %s", main, assist, "к");
    }

    private String standardLinePenalty(PlayerPosition[] firstTeam) {
        int main = 0;
        int assist = 0;
        for (PlayerPosition player : firstTeam) {
            if (player.isPenalty()) {
                main = player.getNumber();
            }
            if (player.isPenaltyAssistant()) {
                assist = player.getNumber();
            }
        }
        return String.format("%d %d %s", main, assist, "п");
    }

    private String standardLineFreeKick(PlayerPosition[] firstTeam) {
        int main = 0;
        int assist = 0;
        for (PlayerPosition player : firstTeam) {
            if (player.isDirectFreekick()) {
                main = player.getNumber();
            }
            if (player.isDirectFreekickAssistant()) {
                assist = player.getNumber();
            }
        }
        return String.format("%d %d %s", main, assist, "ш");
    }

    private String standardLineIndirectFreeKick(PlayerPosition[] firstTeam) {
        int main = 0;
        int assist = 0;
        for (PlayerPosition player : firstTeam) {
            if (player.isIndirectFreekick()) {
                main = player.getNumber();
            }
            if (player.isIndirectFreekickAssistant()) {
                assist = player.getNumber();
            }
        }
        return String.format("%d %d %s", main, assist, "с");
    }

    private String standardLineLeftCorner(PlayerPosition[] firstTeam) {
        int main = 0;
        int assist = 0;
        for (PlayerPosition player : firstTeam) {
            if (player.isLeftCorner()) {
                main = player.getNumber();
            }
            if (player.isLeftCornerAssistant()) {
                assist = player.getNumber();
            }
        }
        return String.format("%d %d %s", main, assist, "ул");
    }

    private String standardLineRightCorner(PlayerPosition[] firstTeam) {
        int main = 0;
        int assist = 0;
        for (PlayerPosition player : firstTeam) {
            if (player.isRightCorner()) {
                main = player.getNumber();
            }
            if (player.isRightCornerAssistant()) {
                assist = player.getNumber();
            }
        }
        return String.format("%d %d %s", main, assist, "уп");
    }

    private StringBuilder formatHalftimeTeamTackticChanges(TeamTactics halftimeTeamTacticChanges) {
        StringBuilder sb = new StringBuilder();
        sb.append(halftimeTeamTacticChanges.getMinimumDifference()).append(" ");
        sb.append(halftimeTeamTacticChanges.getMaximumDifference()).append(" ");
        sb.append(halftimeTeamTacticChanges.getTeamTactic()).append(" ");
        sb.append(halftimeTeamTacticChanges.getTeamHardness()).append(" ");
        sb.append(halftimeTeamTacticChanges.getTeamStyle());
        return sb;
    }
}
