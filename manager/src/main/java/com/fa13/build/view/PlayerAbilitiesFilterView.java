package com.fa13.build.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Spinner;

import com.fa13.build.model.TransferPlayerFilter;
import com.fa13.build.view.MainWindow.FontType;
import com.fa13.build.view.MainWindow.ImageType;

public class PlayerAbilitiesFilterView extends Composite {

    private Group abilityfilterPanel;
    private boolean filterReseting;
    private TransferPlayerFilter playerFilter = new TransferPlayerFilter();
    private Label speedLabel;
    private Spinner speedSpinner1;
    private Spinner speedSpinner2;
    private Label headingLabel;
    private Spinner headingSpinner1;
    private Spinner headingSpinner2;
    private Label fitnessLabel;
    private Spinner staminaSpinner1;
    private Spinner staminaSpinner2;
    private Label shootingLabel;
    private Spinner shootingSpinner1;
    private Spinner shootingSpinner2;
    private Label passingLabel;
    private Spinner passingSpinner1;
    private Spinner passingSpinner2;
    private Label dribblingLabel;
    private Spinner dribblingSpinner1;
    private Spinner dribblingSpinner2;
    private Label crossingLabel;
    private Spinner crossingSpinner1;
    private Spinner crossingSpinner2;
    private Label handlingLabel;
    private Spinner handlingSpinner1;
    private Spinner handlingSpinner2;
    private Label tacklingLabel;
    private Spinner tacklingSpinner1;
    private Spinner tacklingSpinner2;
    private Label reflexesLabel;
    private Spinner reflexesSpinner1;
    private Spinner reflexesSpinner2;

    public PlayerAbilitiesFilterView(Composite parent, int style) {
        super(parent, style);
        setLayout(new GridLayout(1, false));
        initComponents();
    }

    private void initComponents() {
        abilityfilterPanel = new Group(this, SWT.NONE);
        abilityfilterPanel.setText(MainWindow.getMessage("filter.ability"));
        abilityfilterPanel.setFont(MainWindow.getSharedFont(FontType.GROUP_TITLE));
        abilityfilterPanel.setLayout(new GridLayout(6, false));

        GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
        abilityfilterPanel.setLayoutData(data);
        abilityfilterPanel.addMenuDetectListener(new MenuDetectListener() {
            @Override
            public void menuDetected(MenuDetectEvent e) {
                Menu menu = new Menu(abilityfilterPanel);
                MenuItem item = new MenuItem(menu, SWT.NONE);
                item.setImage(MainWindow.getSharedImage(ImageType.CLEAR));
                item.setText(MainWindow.getMessage("ResetFilter"));
                item.addSelectionListener(new SelectionAdapter() {
                    public void widgetSelected(SelectionEvent e) {
                        resetAbilityFilter();
                    }
                });
                menu.setLocation(e.x, e.y);
                menu.setVisible(true);
            }
        });

        // speed
        speedLabel = new Label(abilityfilterPanel, SWT.NONE);
        speedLabel.setText(MainWindow.getMessage("filter.speed"));
        GridData gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        speedLabel.setLayoutData(gridData);

        speedSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        speedSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        speedSpinner1.setLayoutData(gridData);
        speedSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.SPEED, 1));

        speedSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        speedSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        speedSpinner2.setLayoutData(gridData);
        speedSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.SPEED, 2));
        // heading
        headingLabel = new Label(abilityfilterPanel, SWT.NONE);
        headingLabel.setText(MainWindow.getMessage("filter.heading"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        headingLabel.setLayoutData(gridData);

        headingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        headingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        headingSpinner1.setLayoutData(gridData);
        headingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.HEAD, 1));

        headingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        headingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        headingSpinner2.setLayoutData(gridData);
        headingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.HEAD, 2));
        // fitness
        fitnessLabel = new Label(abilityfilterPanel, SWT.NONE);
        fitnessLabel.setText(MainWindow.getMessage("filter.fitness"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        fitnessLabel.setLayoutData(gridData);

        staminaSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        staminaSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        staminaSpinner1.setLayoutData(gridData);
        staminaSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.STAMINA, 1));

        staminaSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        staminaSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        staminaSpinner2.setLayoutData(gridData);
        staminaSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.STAMINA, 2));
        // shooting
        shootingLabel = new Label(abilityfilterPanel, SWT.NONE);
        shootingLabel.setText(MainWindow.getMessage("filter.shooting"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        shootingLabel.setLayoutData(gridData);

        shootingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        shootingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        shootingSpinner1.setLayoutData(gridData);
        shootingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.SHOOT, 1));

        shootingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        shootingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        shootingSpinner2.setLayoutData(gridData);
        shootingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.SHOOT, 2));

        // passing
        passingLabel = new Label(abilityfilterPanel, SWT.NONE);
        passingLabel.setText(MainWindow.getMessage("filter.passing"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        passingLabel.setLayoutData(gridData);

        passingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        passingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        passingSpinner1.setLayoutData(gridData);
        passingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.PASS, 1));

        passingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        passingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        passingSpinner2.setLayoutData(gridData);
        passingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.PASS, 2));

        // dribbling
        dribblingLabel = new Label(abilityfilterPanel, SWT.NONE);
        dribblingLabel.setText(MainWindow.getMessage("filter.dribbling"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        dribblingLabel.setLayoutData(gridData);

        dribblingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        dribblingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        dribblingSpinner1.setLayoutData(gridData);
        dribblingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.DRIBBLE, 1));

        dribblingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        dribblingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        dribblingSpinner2.setLayoutData(gridData);
        dribblingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.DRIBBLE, 2));
        // crossing
        crossingLabel = new Label(abilityfilterPanel, SWT.NONE);
        crossingLabel.setText(MainWindow.getMessage("filter.crossing"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        crossingLabel.setLayoutData(gridData);

        crossingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        crossingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        crossingSpinner1.setLayoutData(gridData);
        crossingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.CROSS, 1));

        crossingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        crossingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        crossingSpinner2.setLayoutData(gridData);
        crossingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.CROSS, 2));
        // handling
        handlingLabel = new Label(abilityfilterPanel, SWT.NONE);
        handlingLabel.setText(MainWindow.getMessage("filter.handling"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        handlingLabel.setLayoutData(gridData);

        handlingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        handlingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        handlingSpinner1.setLayoutData(gridData);
        handlingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.HANDLING, 1));

        handlingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        handlingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        handlingSpinner2.setLayoutData(gridData);
        handlingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.HANDLING, 2));

        // tackling
        tacklingLabel = new Label(abilityfilterPanel, SWT.NONE);
        tacklingLabel.setText(MainWindow.getMessage("filter.tackling"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        tacklingLabel.setLayoutData(gridData);

        tacklingSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        tacklingSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        tacklingSpinner1.setLayoutData(gridData);
        tacklingSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.TACKLE, 1));

        tacklingSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        tacklingSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        tacklingSpinner2.setLayoutData(gridData);
        tacklingSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.TACKLE, 2));
        // reflexes
        reflexesLabel = new Label(abilityfilterPanel, SWT.NONE);
        reflexesLabel.setText(MainWindow.getMessage("filter.reflexes"));
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        reflexesLabel.setLayoutData(gridData);

        reflexesSpinner1 = new Spinner(abilityfilterPanel, SWT.BORDER);
        reflexesSpinner1.setValues(20, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        reflexesSpinner1.setLayoutData(gridData);
        reflexesSpinner1.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.REFLEXES, 1));

        reflexesSpinner2 = new Spinner(abilityfilterPanel, SWT.BORDER);
        reflexesSpinner2.setValues(299, 20, 299, 0, 1, 10);
        gridData = new GridData(SWT.LEFT, SWT.LEFT, true, false);
        reflexesSpinner2.setLayoutData(gridData);
        reflexesSpinner2.addSelectionListener(new FilterSpinnerSelectionListener(FilterSpinner.REFLEXES, 2));

    }

    private void resetAbilityFilter() {
        //to avoid reaction on spinner's selection events
        filterReseting = true;
        getPlayerFilter().resetAbilitiesFilter();

        speedSpinner1.setValues(getPlayerFilter().getSpeed1(), 20, 299, 0, 1, 10);
        speedSpinner2.setValues(getPlayerFilter().getSpeed2(), 20, 299, 0, 1, 10);

        staminaSpinner1.setValues(getPlayerFilter().getStamina1(), 20, 299, 0, 1, 10);
        staminaSpinner2.setValues(getPlayerFilter().getStamina2(), 20, 299, 0, 1, 10);

        passingSpinner1.setValues(getPlayerFilter().getPassing1(), 20, 299, 0, 1, 10);
        passingSpinner2.setValues(getPlayerFilter().getPassing2(), 20, 299, 0, 1, 10);

        crossingSpinner1.setValues(getPlayerFilter().getCrossing1(), 20, 299, 0, 1, 10);
        crossingSpinner2.setValues(getPlayerFilter().getCrossing2(), 20, 299, 0, 1, 10);

        tacklingSpinner1.setValues(getPlayerFilter().getTackling1(), 20, 299, 0, 1, 10);
        tacklingSpinner2.setValues(getPlayerFilter().getTackling2(), 20, 299, 0, 1, 10);

        headingSpinner1.setValues(getPlayerFilter().getHeading1(), 20, 299, 0, 1, 10);
        headingSpinner2.setValues(getPlayerFilter().getHeading2(), 20, 299, 0, 1, 10);

        shootingSpinner1.setValues(getPlayerFilter().getShooting1(), 20, 299, 0, 1, 10);
        shootingSpinner2.setValues(getPlayerFilter().getShooting2(), 20, 299, 0, 1, 10);

        dribblingSpinner1.setValues(getPlayerFilter().getDribbling1(), 20, 299, 0, 1, 10);
        dribblingSpinner2.setValues(getPlayerFilter().getDribbling2(), 20, 299, 0, 1, 10);

        handlingSpinner1.setValues(getPlayerFilter().getHandling1(), 20, 299, 0, 1, 10);
        handlingSpinner2.setValues(getPlayerFilter().getHandling2(), 20, 299, 0, 1, 10);

        reflexesSpinner1.setValues(getPlayerFilter().getReflexes1(), 20, 299, 0, 1, 10);
        reflexesSpinner2.setValues(getPlayerFilter().getReflexes2(), 20, 299, 0, 1, 10);

        filterReseting = false;
        redraw();
    }

    public TransferPlayerFilter getPlayerFilter() {
        return playerFilter;
    }

    public void setPlayerFilter(TransferPlayerFilter playerFilter) {
        this.playerFilter = playerFilter;
    }

    public enum FilterSpinner {
        AGE, WAGE, HEALTH, TICKETS, PRICE, STRENGTH, TALENT, EXPERIANCE, SPEED, STAMINA, PASS, CROSS, TACKLE, HEAD, SHOOT, DRIBBLE, HANDLING, REFLEXES;
    }

    public class FilterSpinnerSelectionListener extends SelectionAdapter {

        private FilterSpinner filterSpinner;
        private int spinnerNumber;

        public FilterSpinnerSelectionListener(FilterSpinner filterSpinner, int spinnerNumber) {
            this.filterSpinner = filterSpinner;
            this.spinnerNumber = spinnerNumber;
        }

        @Override
        public void widgetSelected(SelectionEvent e) {
            if (filterReseting) {
                return;
            }
            switch (filterSpinner) {

                case CROSS:
                    if (spinnerNumber == 1) {
                        getPlayerFilter().setCrossing1(crossingSpinner1.getSelection());
                    } else {
                        getPlayerFilter().setCrossing2(crossingSpinner2.getSelection());
                    }
                    break;
                case DRIBBLE:
                    if (spinnerNumber == 1) {
                        getPlayerFilter().setDribbling1(dribblingSpinner1.getSelection());
                    } else {
                        getPlayerFilter().setDribbling2(dribblingSpinner2.getSelection());
                    }
                    break;

                case HANDLING:
                    if (spinnerNumber == 1) {
                        getPlayerFilter().setHandling1(handlingSpinner1.getSelection());
                    } else {
                        getPlayerFilter().setHandling2(handlingSpinner2.getSelection());
                    }
                    break;
                case HEAD:
                    if (spinnerNumber == 1) {
                        getPlayerFilter().setHeading1(headingSpinner1.getSelection());
                    } else {
                        getPlayerFilter().setHeading2(headingSpinner2.getSelection());
                    }
                    break;

                case PASS:
                    if (spinnerNumber == 1) {
                        getPlayerFilter().setPassing1(passingSpinner1.getSelection());
                    } else {
                        getPlayerFilter().setPassing2(passingSpinner2.getSelection());
                    }
                    break;

                case REFLEXES:
                    if (spinnerNumber == 1) {
                        getPlayerFilter().setReflexes1(reflexesSpinner1.getSelection());
                    } else {
                        getPlayerFilter().setReflexes2(reflexesSpinner2.getSelection());
                    }
                    break;
                case SHOOT:
                    if (spinnerNumber == 1) {
                        getPlayerFilter().setShooting1(shootingSpinner1.getSelection());
                    } else {
                        getPlayerFilter().setShooting2(shootingSpinner2.getSelection());
                    }
                    break;
                case SPEED:
                    if (spinnerNumber == 1) {
                        getPlayerFilter().setSpeed1(speedSpinner1.getSelection());
                    } else {
                        getPlayerFilter().setSpeed2(speedSpinner2.getSelection());
                    }
                    break;
                case STAMINA:
                    if (spinnerNumber == 1) {
                        getPlayerFilter().setStamina1(staminaSpinner1.getSelection());
                    } else {
                        getPlayerFilter().setStamina2(staminaSpinner2.getSelection());
                    }
                    break;

                case TACKLE:
                    if (spinnerNumber == 1) {
                        getPlayerFilter().setTackling1(tacklingSpinner1.getSelection());
                    } else {
                        getPlayerFilter().setTackling2(tacklingSpinner2.getSelection());
                    }
                    break;

                default:
                    break;
            }

        }

        @Override
        public void widgetDefaultSelected(SelectionEvent e) {
            widgetSelected(e);
        }

    }

}