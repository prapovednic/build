package com.fa13.build.view.game;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;

import com.fa13.build.model.game.GameForm;
import com.fa13.build.model.game.PlayerPosition;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.utils.NameDialog;
import com.fa13.build.view.MainWindow;
import com.fa13.build.view.MainWindow.ImageType;
import com.fa13.build.view.game.SchemesModel.ISchemesModelListener;
import com.fa13.build.view.game.SchemesModel.SchemeChangeType;

public class SchemeControlPanel extends Composite {

    public static final String SCHEME_FILE_NAME = "Schemes.txt";

    private Combo schemeCombo;

    private Button saveSchemeButton;

    private Button deleteSchemeButton;

    private Button resetSchemeButton;

    private Composite parentInstance;
    
    private Composite mainPanel;

    private SchemesModel schemesModel;
    
    public SchemeControlPanel(Composite parent, SchemesModel schemesModel) {
        super(parent, parent.getStyle());
        this.schemesModel = schemesModel;
        parentInstance = parent;
        createGUI(parent);
    }

    public void initSchemeComboData() {
        getSchemeCombo().removeAll();
        for (Iterator<Entry<String, PlayerPosition[]>> iterator = schemesModel.getSchemesTemplates().entrySet().iterator(); iterator.hasNext();) {
            Entry<String, PlayerPosition[]> entry = (Entry<String, PlayerPosition[]>) iterator.next();
            getSchemeCombo().add(entry.getKey());
            getSchemeCombo().setData(entry.getKey(), entry.getValue());
        }
    }
    
    private void createGUI(Composite parent) {
        
        mainPanel = new Composite(this, SWT.NONE);
        FormData data = new FormData();
        data.top = new FormAttachment(0, 0);
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        
        mainPanel.setLayoutData(data);
        
        schemeCombo = new Combo(mainPanel, SWT.DROP_DOWN | SWT.READ_ONLY);
        initSchemeComboData();

        getSchemeCombo().select(-1);
        mainPanel.setLayout(new FormLayout());
        saveSchemeButton = new Button(mainPanel, SWT.PUSH);
        saveSchemeButton.setImage(MainWindow.getSharedImage(ImageType.SAVE));
        saveSchemeButton.setToolTipText(MainWindow.getMessage("global.save"));

        deleteSchemeButton = new Button(mainPanel, SWT.PUSH);
        deleteSchemeButton.setToolTipText(MainWindow.getMessage("DeleteSchemeButton"));
        deleteSchemeButton.setImage(MainWindow.getSharedImage(ImageType.ERROR));

        resetSchemeButton = new Button(mainPanel, SWT.PUSH);
        resetSchemeButton.setImage(MainWindow.getSharedImage(ImageType.CLEAR));
        resetSchemeButton.setToolTipText(MainWindow.getMessage("resetSchemesToDefault"));

        data = new FormData();
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(0, 0);
        resetSchemeButton.setLayoutData(data);
        
        data = new FormData();
        data.right = new FormAttachment(resetSchemeButton, -4);
        data.top = new FormAttachment(0, 0);
        deleteSchemeButton.setLayoutData(data);
        
        data = new FormData();
        data.right = new FormAttachment(deleteSchemeButton, -4);
        data.top = new FormAttachment(0, 0);
        saveSchemeButton.setLayoutData(data);
        
        data = new FormData();
        data.left = new FormAttachment(0, 10);
        data.right = new FormAttachment(saveSchemeButton, -4);
        data.top = new FormAttachment(0, 0);
        schemeCombo.setLayoutData(data);
        mainPanel.pack();
        mainPanel.layout();
        
        //add button handlers
        saveSchemeButton.addSelectionListener(new SelectionListener() {
            
            @Override
            public void widgetSelected(SelectionEvent e) {
                
                NameDialog dlg = new NameDialog(getShell());
                dlg.setText(MainWindow.getMessage("schemeName"));
                dlg.setAllowEmpty(false);
                if (dlg.open() == SWT.OK) {
                    //check if name unique
                    Object obj = schemesModel.getSchemesTemplates().get(dlg.getName());
                    if (obj != null) {
                        GuiUtils.showErrorMessage(getShell(), MainWindow.getMessage("error"), MainWindow.getMessage("alreadyUsedName"));
                    } else {
                        //TODO check how many players?
                        //TODO READ current scheme
                        schemesModel.addScheme(dlg.getName(), GameForm.getInstance().getFirstTeam());
                    }
                }
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });
        
        deleteSchemeButton.addSelectionListener(new SelectionListener() {
            
            @Override
            public void widgetSelected(SelectionEvent e) {
                
                int selected = schemeCombo.getSelectionIndex();
                
                if (selected == -1) {
                    return;
                }
                
                MessageBox dialog = new MessageBox(getShell(), SWT.ICON_WARNING | SWT.OK | SWT.CANCEL);
                dialog.setText(MainWindow.getMessage("confirmation"));
                String mes = MainWindow.getMessage("deleteSchemeConfirm") + " [" + schemeCombo.getItem(selected) +"] ?";
                dialog.setMessage( mes );
                if (dialog.open() == SWT.OK) {
                    //do delete
                    String schemeName = schemeCombo.getItem(selected);
                    schemesModel.removeScheme(schemeName);
                }     
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });
        
        resetSchemeButton.addSelectionListener(new SelectionListener() {
            
            @Override
            public void widgetSelected(SelectionEvent e) {
                MessageBox dialog = new MessageBox(getShell(), SWT.ICON_WARNING | SWT.OK | SWT.CANCEL);
                dialog.setText(MainWindow.getMessage("confirmation"));
                dialog.setMessage(MainWindow.getMessage("resetSchemesConfirm"));
                if (dialog.open() == SWT.OK) {
                    schemesModel.resetSchemes();
                }
                
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        schemesModel.addChangeListener(new ISchemesModelListener() {
            
            @Override
            public void handeSchemeChanged(SchemeChangeType eventType, String schemeName, PlayerPosition[] schemeData) {
                
                if (eventType == SchemeChangeType.SELECTED) {
                    int index = GuiUtils.getComboItemIndex(schemeCombo, schemeName);
                    schemeCombo.select(index);
                } else if (eventType == SchemeChangeType.ADDED) {
                    schemeCombo.setData(schemeName, GameForm.getInstance().getFirstTeam());
                    initSchemeComboData();
                } else {
                    initSchemeComboData();
                }
                
            }
        });
        
    }

    public Combo getSchemeCombo() {
        return schemeCombo;
    }

    public void setSchemeCombo(Combo schemeCombo) {
        this.schemeCombo = schemeCombo;
    }

    public void updateMessages() {
        deleteSchemeButton.setToolTipText(MainWindow.getMessage("DeleteSchemeButton"));
        saveSchemeButton.setToolTipText(MainWindow.getMessage("global.save"));
        resetSchemeButton.setToolTipText(MainWindow.getMessage("resetSchemesToDefault"));
    }

}