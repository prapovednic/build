package com.fa13.build.view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Spinner;

public class DaysOfRestComposite extends Composite {

    Label restLabel;
    Slider restScale;
    Spinner restSpinner;
    TeamUIItem uiItem = null;
    static final int REST_SLIDER_MAX = 14;

    public DaysOfRestComposite(Composite parent, int style, TeamUIItem uiItem) {
        super(parent, style);
        this.uiItem = uiItem;

        setLayout(new FormLayout());

        restLabel = new Label(this, SWT.NONE);
        FormData data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.top = new FormAttachment(0, 0);
        restLabel.setText(MainWindow.getMessage("PTDaysOfRest") + ": ");

        restSpinner = new Spinner(this, SWT.LEFT | SWT.BORDER);
        data = new FormData();
        data.left = new FormAttachment(restLabel, 5);
        data.top = new FormAttachment(0, 0);
        restSpinner.setLayoutData(data);
        restSpinner.setValues(0, 0, REST_SLIDER_MAX, 0, 1, 5);
        restSpinner.addSelectionListener(saDaysOfRest);
        restSpinner.setTextLimit(3);
        
        restScale = new Slider(this, SWT.NONE);

        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(restSpinner, 5);
        restScale.setLayoutData(data);

        restScale.setMinimum(0);
        restScale.setMaximum(REST_SLIDER_MAX + 10);
        restScale.setPageIncrement(5);
        restScale.setIncrement(1);
        restScale.addSelectionListener(saDaysOfRest);



    }

    public void setDaysOfRest(int daysOfRest) {
        if (restSpinner != null) {
            restSpinner.setValues(daysOfRest, 0, REST_SLIDER_MAX, 0, 1, 5);
            restScale.setValues(daysOfRest, 0, REST_SLIDER_MAX, 0, 1, 5);
            if (uiItem != null) {
                uiItem.updateDaysOfRest(daysOfRest);
            }
        }
    }

    public int getDaysOfRest() {
        if (restSpinner != null) {
            return getIntegerValue(restSpinner.getText());
        } else {
            return 0;
        }
    }

    public static int getIntegerValue(String st) {
        if (st == null || st.isEmpty()) {
            return 0;
        }
        return Integer.valueOf(st);
    }

    private SelectionAdapter saDaysOfRest = new SelectionAdapter() {

        public void widgetSelected(SelectionEvent e) {

            int val = e.getSource() == restScale ? restScale.getSelection() : restSpinner.getSelection();
            restSpinner.setSelection(val);
            restScale.setSelection(val);
            if (uiItem != null) {
                uiItem.updateDaysOfRest(val);
            }
        }

    };

    public void updateMessages() {
        restLabel.setText(MainWindow.getMessage("PTDaysOfRest") + ": ");
        layout();
    }
}
