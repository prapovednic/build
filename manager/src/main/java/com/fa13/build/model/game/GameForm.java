package com.fa13.build.model.game;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.fa13.build.model.Team;

public class GameForm {

    private static GameForm instance = new GameForm();

    private String tournamentID;
    private String gameType;
    private String teamID;
    private String password;

    private int selectedSchemeIndex = -1;

    private TeamTactics startTactics;
    private CoachSettings teamCoachSettings;
    private PlayerPosition[] firstTeam;

    private int[] bench = new int[7];
    private int price;

    private int attackTime;
    private int attackMin;
    private int attackMax = -1;
    private int defenceTime;
    private int defenceMin;
    private int defenceMax = -1;

    private PlayerSubstitution[] substitutions = new PlayerSubstitution[25];
    private SubstitutionPreferences substitutionPreferences = SubstitutionPreferences.BY_POSITION;
    private TeamTactics[] halftimeChanges = new TeamTactics[5];

    private PlayerSubstitution activeCoordSubtition = new PlayerSubstitution();
    
    public static synchronized GameForm newInstance() {
        instance = new GameForm();
        return instance;
    }

    public static synchronized GameForm getInstance() {
        return instance;
    }

    private GameForm() {
        firstTeam = new PlayerPosition[11];
        for (int i = 0; i < 11; i++) {
            firstTeam[i] = new PlayerPosition();
        }
    }

    public int getSelectedSchemeIndex() {
        return selectedSchemeIndex;
    }

    public void setSelectedSchemeIndex(int selectedIndex) {
        this.selectedSchemeIndex = selectedIndex;
    }

    public PlayerSubstitution[] getSubstitutions() {
        return substitutions;
    }

    public void setSubstitutions(PlayerSubstitution[] substitutions) {
        this.substitutions = substitutions;
    }

    public SubstitutionPreferences getSubstitutionPreferences() {
        return substitutionPreferences;
    }

    public void setSubstitutionPreferences(SubstitutionPreferences substitutionPreferences) {
        this.substitutionPreferences = substitutionPreferences;
    }

    public TeamTactics[] getHalftimeChanges() {
        return halftimeChanges;
    }

    public void setHalftimeChanges(TeamTactics[] halftimeChanges) {
        this.halftimeChanges = halftimeChanges;
    }

    public String getTournamentID() {
        return tournamentID;
    }

    public void setTournamentID(String tournamentID) {
        this.tournamentID = tournamentID;
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getTeamID() {
        return teamID;
    }

    public void setTeamID(String teamID) {
        this.teamID = teamID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public TeamTactics getStartTactics() {
        return startTactics;
    }

    public void setStartTactics(TeamTactics startTactics) {
        this.startTactics = startTactics;
    }

    public CoachSettings getTeamCoachSettings() {
        return teamCoachSettings;
    }

    public void setTeamCoachSettings(CoachSettings teamCoachSettings) {
        this.teamCoachSettings = teamCoachSettings;
    }

    public PlayerPosition[] getFirstTeam() {
        return firstTeam;
    }

    public void setFirstTeam(PlayerPosition[] firstTeam) {
        this.firstTeam = firstTeam;
    }

    public int[] getBench() {
        return bench;
    }

    public void setBench(int[] bench) {
        this.bench = bench;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAttackTime() {
        return attackTime;
    }

    public void setAttackTime(int attackTime) {
        this.attackTime = attackTime;
    }

    public int getAttackMin() {
        return attackMin;
    }

    public void setAttackMin(int attackMin) {
        this.attackMin = attackMin;
    }

    public int getAttackMax() {
        return attackMax;
    }

    public void setAttackMax(int attackMax) {
        this.attackMax = attackMax;
    }

    public int getDefenceTime() {
        return defenceTime;
    }

    public void setDefenceTime(int defenceTime) {
        this.defenceTime = defenceTime;
    }

    public int getDefenceMin() {
        return defenceMin;
    }

    public void setDefenceMin(int defenceMin) {
        this.defenceMin = defenceMin;
    }

    public int getDefenceMax() {
        return defenceMax;
    }

    public void setDefenceMax(int defenceMax) {
        this.defenceMax = defenceMax;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

    public PlayerSubstitution getActiveCoordSubtition() {
        return activeCoordSubtition;
    }

    public void setActiveCoordSubtition(PlayerSubstitution activeCoordSubtition) {
        this.activeCoordSubtition = activeCoordSubtition;
    }

    public void clearIncorrectData(Team currentTeam) {
        //clear absent players from bench
        for (int i = 0; i < bench.length; i++) {
            if (currentTeam.getPlayerByNumber(bench[i]) == null) {
                bench[i] = 0;
            }
        }
        //TODO clear absent players from startup line?
        //TODO clear absent players from subs?
        //clearing will not give us any advance
        //because server will reject bid in all cases if absent players are used
        //no metter what number it will be
    }
}
