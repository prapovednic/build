package com.fa13.build.model.training;

import java.util.ArrayList;
import java.util.List;

public class TrainingForm {
    private String teamID;
    private String password;
    private int minTalent;
    private boolean scouting = true;
    private List<PlayerTraining> players = new ArrayList<PlayerTraining>(15);

    public TrainingForm() {
    }

    public TrainingForm(String password, int minTalent, boolean scouting, List<PlayerTraining> players) {
        this.password = password;
        this.minTalent = minTalent;
        this.scouting = scouting;
        this.players = players;
    }

    public String getTeamID() {
        return teamID;
    }

    public void setTeamID(String teamID) {
        this.teamID = teamID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getMinTalent() {
        return minTalent;
    }

    public void setMinTalent(int minTalent) {
        this.minTalent = minTalent;
    }

    public boolean isScouting() {
        return scouting;
    }

    public boolean getScouting() {
        return this.scouting;
    }

    public void setScouting(boolean scouting) {
        this.scouting = scouting;
    }

    public List<PlayerTraining> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerTraining> players) {
        this.players = players;
    }
}
