package com.fa13.build.model.game;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class TeamTactics {

    Tactic teamTactic;
    Hardness teamHardness;
    Style teamStyle;

    int minimumDifference;
    int maximumDifference;

    public TeamTactics(Tactic teamTactic, Hardness teamHardness, Style teamStyle,
                       int minimumDifference, int maximumDifference) {
        this.teamTactic = teamTactic;
        this.teamHardness = teamHardness;
        this.teamStyle = teamStyle;
        this.minimumDifference = minimumDifference;
        this.maximumDifference = maximumDifference;
    }

    public Tactic getTeamTactic() {
        return teamTactic;
    }

    public void setTeamTactic(Tactic teamTactic) {
        this.teamTactic = teamTactic;
    }

    public Hardness getTeamHardness() {
        return teamHardness;
    }

    public void setTeamHardness(Hardness teamHardness) {
        this.teamHardness = teamHardness;
    }

    public Style getTeamStyle() {
        return teamStyle;
    }

    public void setTeamStyle(Style teamStyle) {
        this.teamStyle = teamStyle;
    }

    public int getMinimumDifference() {
        return minimumDifference;
    }

    public void setMinimumDifference(int minimumDifference) {
        this.minimumDifference = minimumDifference;
    }

    public int getMaximumDifference() {
        return maximumDifference;
    }

    public void setMaximumDifference(int maximumDifference) {
        this.maximumDifference = maximumDifference;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
