package com.fa13.build.model;

public class ExchangeBid extends PlayerBid {

    public ExchangeBid(int number, int otherPlayerNumber, int surcharge, String otherTeam) {
        this.number = number;
        this.otherPlayerNumber = otherPlayerNumber;
        this.surcharge = surcharge;
        this.otherTeam = otherTeam;
        this.empty = false;
    }

    private int surcharge;
    
    public int getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(int surcharge) {
        this.surcharge = surcharge;
    }

    private int otherPlayerNumber;
    
    public int getOtherPlayerNumber() {
        return otherPlayerNumber;
    }

    public void setOtherPlayerNumber(int otherPlayerNumber) {
        this.otherPlayerNumber = otherPlayerNumber;
    }
    
    private String otherTeam;
    
    public String getOtherTeam() {
        return otherTeam;
    }
    public void setOtherTeam(String otherTeam) {
        this.otherTeam = otherTeam;
    }
    
    public String toString() {
        return "Б " + number + " " + otherPlayerNumber + " " + surcharge + " " + otherTeam;
    }
    
    public boolean isValid() {
        
        if (number < 1 || otherPlayerNumber<1 || otherTeam == null || otherTeam.isEmpty())
            return false;
        else
            return true;
        
    }
    
}