package com.fa13.build.model;

public class Club {
    String password;
    String email = null;
    int icq = -1;
    private String teamID;

    public static enum TriAction {
        TR_NOTHING, TR_BUILD, TR_REPAIR
    }

    TriAction stadiumAction = TriAction.TR_NOTHING;
    int stadium = -1;

    TriAction sportBaseAction = TriAction.TR_NOTHING;
    int sportBase = -1;

    TriAction sportSchoolAction = TriAction.TR_NOTHING;
    boolean school = false;

    int coachCoef = 0;
    int coachGK = -1;
    int coachDef = -1;
    int coachMid = -1;
    int coachFw = -1;
    int coachFitness = -1;
    int coachMorale = -1;
    int doctorLevel = -1;
    int doctorCount = -1;
    int scoutLevel = -1;
    int color1 = -1;
    int color2 = -1;
    int color3 = -1;
    int color4 = -1;
    
    private int coachCoefDiff = 0;
    private boolean changeCoachCoef = false;
    private boolean changeCoachGK = false;
    private boolean changeCoachDef = false;
    private boolean changeCoachMid = false;
    private boolean changeCoachFw = false;
    private boolean changeCoachFitness = false;
    private boolean changeCoachMorale = false;
    private boolean changeDoctor = false;
    private boolean changeScout = false;

    private boolean changeUniformColors = false;
    private boolean changeColor1 = false;
    private boolean changeColor2 = false;
    private boolean changeColor3 = false;
    private boolean changeColor4 = false;
    
    public Club() {

    }

    public Club(String password, String email, int icq, TriAction stadiumAction, int stadium, TriAction sportBaseAction, int sportBase,
            TriAction sportSchoolAction, boolean school, int coachCoef, int coachGK, int coachDef, int coachMid, int coachFor, int coachFitness,
            int coachMorale, int doctorLevel, int doctorCount, int scoutLevel, int color1, int color2, int color3, int color4) {
        this.password = password;
        this.email = email;
        this.icq = icq;
        this.stadiumAction = stadiumAction;
        this.stadium = stadium;
        this.sportBaseAction = sportBaseAction;
        this.sportBase = sportBase;
        this.sportSchoolAction = sportSchoolAction;
        this.school = school;
        this.coachCoef = coachCoef;
        this.coachGK = coachGK;
        this.coachDef = coachDef;
        this.coachMid = coachMid;
        this.coachFw = coachFor;
        this.coachFitness = coachFitness;
        this.coachMorale = coachMorale;
        this.doctorLevel = doctorLevel;
        this.doctorCount = doctorCount;
        this.scoutLevel = scoutLevel;
        this.color1 = color1;
        this.color2 = color2;
        this.color3 = color3;
        this.color4 = color4;
    }

    public String getTeamID() {
        return teamID;
    }

    public void setTeamID(String teamID) {
        this.teamID = teamID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIcq() {
        return icq;
    }

    public void setIcq(int icq) {
        this.icq = icq;
    }

    public TriAction getStadiumAction() {
        return stadiumAction;
    }

    public void setStadiumAction(TriAction stadiumAction) {
        this.stadiumAction = stadiumAction;
    }

    public int getStadium() {
        return stadium;
    }

    public void setStadium(int stadium) {
        this.stadium = stadium;
    }

    public TriAction getSportBaseAction() {
        return sportBaseAction;
    }

    public void setSportBaseAction(TriAction sportBaseAction) {
        this.sportBaseAction = sportBaseAction;
    }

    public int getSportBase() {
        return sportBase;
    }

    public void setSportBase(int sportBase) {
        this.sportBase = sportBase;
    }

    public TriAction getSportSchoolAction() {
        return sportSchoolAction;
    }

    public void setSportSchoolAction(TriAction sportSchoolAction) {
        this.sportSchoolAction = sportSchoolAction;
    }

    public boolean isSchool() {
        return school;
    }

    public void setSchool(boolean school) {
        this.school = school;
    }

    public int getCoachCoef() {
        return coachCoef;
    }

    public void setCoachCoef(int coachCoef) {
        this.coachCoef = coachCoef;
    }

    public int getCoachGK() {
        return coachGK;
    }

    public void setCoachGK(int coachGK) {
        this.coachGK = coachGK;
    }

    public int getCoachDef() {
        return coachDef;
    }

    public void setCoachDef(int coachDef) {
        this.coachDef = coachDef;
    }

    public int getCoachMid() {
        return coachMid;
    }

    public void setCoachMid(int coachMid) {
        this.coachMid = coachMid;
    }

    public int getCoachFw() {
        return coachFw;
    }

    public void setCoachFw(int coachFor) {
        this.coachFw = coachFor;
    }

    public int getCoachFitness() {
        return coachFitness;
    }

    public void setCoachFitness(int coachFitness) {
        this.coachFitness = coachFitness;
    }

    public int getCoachMorale() {
        return coachMorale;
    }

    public void setCoachMorale(int coachMorale) {
        this.coachMorale = coachMorale;
    }

    public int getDoctorLevel() {
        return doctorLevel;
    }

    public void setDoctorLevel(int doctorLevel) {
        this.doctorLevel = doctorLevel;
    }

    public int getDoctorCount() {
        return doctorCount;
    }

    public void setDoctorCount(int doctorCount) {
        this.doctorCount = doctorCount;
    }

    public int getScoutLevel() {
        return scoutLevel;
    }

    public void setScoutLevel(int scoutLevel) {
        this.scoutLevel = scoutLevel;
    }

    public int getColor1() {
        return color1;
    }

    public void setColor1(int color1) {
        this.color1 = color1;
    }

    public int getColor2() {
        return color2;
    }

    public void setColor2(int color2) {
        this.color2 = color2;
    }

    public int getColor3() {
        return color3;
    }

    public void setColor3(int color3) {
        this.color3 = color3;
    }

    public int getColor4() {
        return color4;
    }

    public void setColor4(int color4) {
        this.color4 = color4;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void initFromTeam(Team team) {
        if (team == null) {
            return;
        }
        this.teamID = team.id;
        this.email = team.email;
        //this.icq = ?;
        this.stadiumAction = TriAction.TR_NOTHING;
        this.stadium = team.getStadiumCapacity();
        this.sportBaseAction = TriAction.TR_NOTHING;
        this.sportBase = team.getSportbase();
        this.sportSchoolAction = TriAction.TR_NOTHING;
        this.school = team.sportschool;
        this.coachCoef = team.getCoach();
        this.coachGK = team.getGoalkeepersCoach();
        this.coachDef = team.getDefendersCoach();
        this.coachMid = team.getMidfieldersCoach();
        this.coachFw = team.getForwardsCoach();
        this.coachFitness = team.getFitnessCoach();
        this.coachMorale = team.getMoraleCoach();
        this.doctorLevel = team.getDoctorQualification();
        this.doctorCount = team.getDoctorPlayers();
        this.scoutLevel = team.getScout();
        this.color1 = team.getHomeTop();
        this.color2 = team.getHomeBottom();
        this.color3 = team.getAwayTop();
        this.color4 = team.getAwayBottom();

    }

    public void copyFrom(Club otherClub) {
        this.password = otherClub.password;
        this.email = otherClub.email;
        this.icq = otherClub.icq;
        this.stadiumAction = otherClub.stadiumAction;
        this.stadium = otherClub.stadium;
        this.sportBaseAction = otherClub.sportBaseAction;
        this.sportBase = otherClub.sportBase;
        this.sportSchoolAction = otherClub.sportSchoolAction;
        this.school = otherClub.school;
        this.coachCoef = otherClub.coachCoef;
        this.coachGK = otherClub.coachGK;
        this.coachDef = otherClub.coachDef;
        this.coachMid = otherClub.coachMid;
        this.coachFw = otherClub.coachFw;
        this.coachFitness = otherClub.coachFitness;
        this.coachMorale = otherClub.coachMorale;
        this.doctorLevel = otherClub.doctorLevel;
        this.doctorCount = otherClub.doctorCount;
        this.scoutLevel = otherClub.scoutLevel;
        this.color1 = otherClub.color1;
        this.color2 = otherClub.color2;
        this.color3 = otherClub.color3;
        this.color4 = otherClub.color4;
        
        this.changeCoachCoef = otherClub.changeCoachCoef;
        this.changeCoachDef = otherClub.changeCoachDef;
        this.changeCoachFitness = otherClub.changeCoachFitness;
        this.changeCoachFw = otherClub.changeCoachFw;
        this.changeCoachGK = otherClub.changeCoachGK;
        this.changeCoachMid = otherClub.changeCoachMid;
        this.changeCoachMorale = otherClub.changeCoachMorale;
        this.changeColor1 = otherClub.changeColor1;
        this.changeColor2 = otherClub.changeColor2;
        this.changeColor3 = otherClub.changeColor3;
        this.changeColor4 = otherClub.changeColor4;
        this.changeDoctor = otherClub.changeDoctor;
        this.changeScout = otherClub.changeScout;
        this.changeUniformColors = otherClub.changeUniformColors;
        
    }

    public boolean isChangeUniformColors() {
        return changeUniformColors;
    }

    public void setChangeUniformColors(boolean changeUniformColors) {
        this.changeUniformColors = changeUniformColors;
    }

    public boolean isChangeCoachCoef() {
        return changeCoachCoef;
    }

    public void setChangeCoachCoef(boolean changeCoachCoef) {
        this.changeCoachCoef = changeCoachCoef;
    }

    public boolean isChangeCoachGK() {
        return changeCoachGK;
    }

    public void setChangeCoachGK(boolean changeCoachGK) {
        this.changeCoachGK = changeCoachGK;
    }

    public boolean isChangeCoachDef() {
        return changeCoachDef;
    }

    public void setChangeCoachDef(boolean changeCoachDef) {
        this.changeCoachDef = changeCoachDef;
    }

    public boolean isChangeCoachMid() {
        return changeCoachMid;
    }

    public void setChangeCoachMid(boolean changeCoachMid) {
        this.changeCoachMid = changeCoachMid;
    }

    public boolean isChangeCoachFw() {
        return changeCoachFw;
    }

    public void setChangeCoachFw(boolean changeCoachFw) {
        this.changeCoachFw = changeCoachFw;
    }

    public boolean isChangeCoachFitness() {
        return changeCoachFitness;
    }

    public void setChangeCoachFitness(boolean changeCoachFitness) {
        this.changeCoachFitness = changeCoachFitness;
    }

    public boolean isChangeCoachMorale() {
        return changeCoachMorale;
    }

    public void setChangeCoachMorale(boolean changeCoachMorale) {
        this.changeCoachMorale = changeCoachMorale;
    }

    public boolean isChangeDoctor() {
        return changeDoctor;
    }

    public void setChangeDoctor(boolean changeDoctor) {
        this.changeDoctor = changeDoctor;
    }

    public boolean isChangeScout() {
        return changeScout;
    }

    public void setChangeScout(boolean changeScout) {
        this.changeScout = changeScout;
    }
/* not needed for a while
    public void calculateChangedFrom(Club other) {

        setChangeCoachCoef(getCoachCoef() - other.getCoachCoef() != 0);
        setChangeCoachDef(getCoachDef() - other.getCoachDef() != 0);
        setChangeCoachFitness(getCoachFitness() - other.getCoachFitness() != 0);
        setChangeCoachFw(getCoachFw() - other.getCoachFw() != 0);
        setChangeCoachGK(getCoachGK() - other.getCoachGK() != 0);
        setChangeCoachMid(getCoachMid() - other.getCoachMid() != 0);
        setChangeCoachMorale(getCoachMorale() - other.getCoachMorale() != 0);
        setChangeScout(getScoutLevel() - other.getScoutLevel() != 0);
        setChangeDoctor(getDoctorCount() + getDoctorLevel() - other.getDoctorCount() - other.getDoctorLevel() != 0);
        setChangeUniformColors((getColor1() - other.getColor1() != 0) || (getColor2() - other.getColor2() != 0)
                || (getColor3() - other.getColor3() != 0) || (getColor4() - other.getColor4() != 0));
        
    }
*/

    public boolean isChangeColor1() {
        return changeColor1;
    }

    public void setChangeColor1(boolean changeColor1) {
        this.changeColor1 = changeColor1;
    }

    public boolean isChangeColor2() {
        return changeColor2;
    }

    public void setChangeColor2(boolean changeColor2) {
        this.changeColor2 = changeColor2;
    }

    public boolean isChangeColor3() {
        return changeColor3;
    }

    public void setChangeColor3(boolean changeColor3) {
        this.changeColor3 = changeColor3;
    }

    public boolean isChangeColor4() {
        return changeColor4;
    }

    public void setChangeColor4(boolean changeColor4) {
        this.changeColor4 = changeColor4;
    }

    public int getCoachCoefDiff() {
        return coachCoefDiff;
    }

    public void setCoachCoefDiff(int coachCoefDiff) {
        this.coachCoefDiff = coachCoefDiff;
    }
}
