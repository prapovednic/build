package com.fa13.build.controller.io;


import com.fa13.build.model.All;
import com.fa13.build.model.Team;
import com.fa13.build.model.training.PlayerTraining;
import com.fa13.build.model.training.TrainingForm;
import com.fa13.build.utils.ResourceUtils;
import com.fa13.build.view.MainWindow;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MainWindow.class})
public class TrainingWriterTest {
    private ByteArrayOutputStream resultStream = new ByteArrayOutputStream();
    private TrainingWriter writer = new TrainingWriter();

    @Before
    public void setUp() {
        mockStatic(MainWindow.class);
        All all = mock(All.class);
        Team team = mock(Team.class);

        when(MainWindow.getAllInstance()).thenReturn(all);
        when(all.getTeamById("MY")).thenReturn(team);
        when(team.getName()).thenReturn("Моя команда");
    }

    @Test
    public void write_scout_enabled() throws Exception {
        TrainingForm form = createTrainingForm(true, 45);
        writer.write(resultStream, form);
        verifyFormContent("/forms/training-content.txt");
    }

    @Test
    public void write_scout_disabled() throws Exception {
        TrainingForm form = createTrainingForm(false, 45);
        writer.write(resultStream, form);
        verifyFormContent("/forms/training-content-scout-disabled.txt");
    }

    private TrainingForm createTrainingForm(boolean enableScout, int minTalent) {
        List<PlayerTraining> players = new ArrayList<>();
        PlayerTraining playerTraining;

        playerTraining = new PlayerTraining(1);
        playerTraining.setShootingPoints(1);
        players.add(playerTraining);

        playerTraining = new PlayerTraining(2);
        playerTraining.setPassingPoints(2);
        players.add(playerTraining);

        playerTraining = new PlayerTraining(3);
        playerTraining.setCrossPoints(3);
        players.add(playerTraining);

        playerTraining = new PlayerTraining(4);
        playerTraining.setDribblingPoints(4);
        players.add(playerTraining);

        playerTraining = new PlayerTraining(5);
        playerTraining.setTacklingPoints(5);
        players.add(playerTraining);

        playerTraining = new PlayerTraining(6);
        playerTraining.setSpeedPoints(1);
        players.add(playerTraining);

        playerTraining = new PlayerTraining(7);
        playerTraining.setStaminaPoints(2);
        players.add(playerTraining);

        playerTraining = new PlayerTraining(8);
        playerTraining.setHeadingPoints(3);
        players.add(playerTraining);

        playerTraining = new PlayerTraining(9);
        playerTraining.setReflexesPoints(4);
        players.add(playerTraining);

        playerTraining = new PlayerTraining(10);
        playerTraining.setHandlingPoints(5);
        players.add(playerTraining);

        playerTraining = new PlayerTraining(11);
        playerTraining.setFitnessPoints(1);
        players.add(playerTraining);

        playerTraining = new PlayerTraining(12);
        playerTraining.setMoraleFinance(2);
        players.add(playerTraining);

        playerTraining = new PlayerTraining(13);
        playerTraining.setFitnessFinance(3);
        players.add(playerTraining);

        return new TrainingForm("password", minTalent, enableScout, players);
    }

    private void verifyFormContent(String expectedContentFileName) throws IOException {
        String result = resultStream.toString(HashingOutputStream.WIN_CHARSET.name());
        String actualContent = extractMainContent(result);

        String expectedContent = ResourceUtils.getClasspathResourceAsString(expectedContentFileName);

        assertEquals(expectedContent, actualContent);
    }

    private String extractMainContent(String form) {
        String startString = AbstractFormWriter.LINE_BREAK + AbstractFormWriter.LINE_BREAK;
        String endString = "*****";
        int contentStart = form.indexOf(startString);
        int contentEnd = form.indexOf(endString);
        return form.substring(contentStart + startString.length(), contentEnd);
    }
}