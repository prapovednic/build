package com.fa13.build.controller.io;


import com.fa13.build.model.All;
import com.fa13.build.model.Team;
import com.fa13.build.model.game.GameForm;
import com.fa13.build.utils.GameFormBuilder;
import com.fa13.build.utils.ResourceUtils;
import com.fa13.build.view.MainWindow;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MainWindow.class})
public class GameWriterTest {
    private ByteArrayOutputStream resultStream = new ByteArrayOutputStream();
    private GameWriter writer = new GameWriter();

    @Before
    public void setUp() {
        mockStatic(MainWindow.class);
        All all = mock(All.class);
        Team team = mock(Team.class);

        when(MainWindow.getAllInstance()).thenReturn(all);
        when(all.getTeamById("MY")).thenReturn(team);
        when(team.getName()).thenReturn("Моя команда");
    }

    @Test
    public void write_fullForm() throws Exception {
        GameForm form = GameFormBuilder.createGameForm();
        writer.write(resultStream, form);
        verifyFormContent("/forms/game-full.txt");
    }

    private void verifyFormContent(String expectedContentFileName) throws IOException {
        String result = resultStream.toString(HashingOutputStream.WIN_CHARSET.name());
        String actualContent = extractMainContent(result);

        String expectedContent = ResourceUtils.getClasspathResourceAsString(expectedContentFileName);

        assertEquals(expectedContent, actualContent);
    }

    private String extractMainContent(String form) {
        String startString = AbstractFormWriter.LINE_BREAK + AbstractFormWriter.LINE_BREAK;
        String endString = "*****";
        int contentStart = form.indexOf(startString);
        int contentEnd = form.indexOf(endString);
        return form.substring(contentStart + startString.length(), contentEnd);
    }
}